import './style.less';

import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { bindSelf } from '@/utils';
import { Link } from 'react-router-dom';

@inject('user')
@observer
export default class HomePage extends Component {
    state = {
        time: 0
    };

    @bindSelf
    click() {
        this.setState({ time: +new Date() });
    }

    componentDidMount() {
        this.props.user.getUser();
    }

    render() {
        const { data } = this.props.user;
        return (
            <div className="home">
                <div onClick={this.click}>
                    mobx案例
                    {data.username}
                    {this.state.time}
                    <Link to={'/child'}>child</Link>
                </div>
            </div>
        );
    }
}
